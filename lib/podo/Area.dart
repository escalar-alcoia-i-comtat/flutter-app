class Area {
  int id;
  String timestamp;
  int version;
  String displayName;
  String image;
  String kmlAddress;
  String webURL;
  //dynamic zones;

  Area({
    this.id,
    this.timestamp,
    this.version,
    this.displayName,
    this.image,
    this.kmlAddress,
    this.webURL,
    /*this.zones*/
  });

  factory Area.fromJSON(Map<String, dynamic> json) {
    return Area(
      id: json["id"] as int,
      timestamp: json["timestamp"] as String,
      version: json["version"] as int,
      displayName: json["display_name"] as String,
      image: json["image"] as String,
      kmlAddress: json["kml_address"] as String,
      webURL: json["webURL"] as String,
      //zones: json["zones"],
    );
  }
}
