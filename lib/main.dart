import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/AreasLoader.dart';
import 'package:flutter_app/podo/Area.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: Colors.lightGreen.shade700,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        backgroundColor: Colors.lightGreen,
        fontFamily: 'Product Sans',
      ),
      home: HomePageWidget(username: 'User'),
    );
  }
}

class HomePageWidget extends StatefulWidget {
  HomePageWidget({Key key, this.username}) : super(key: key);

  final String username;

  @override
  _HomePageWidgetState createState() => _HomePageWidgetState();
}

class _HomePageWidgetState extends State<HomePageWidget> {
  String areasDownloadProgress = "-";

  void updateAreasProgress(String newProgress) {
    setState(() {
      areasDownloadProgress = newProgress;
    });
  }

  AreasLoader areasLoader;

  _HomePageWidgetState() {
    areasLoader = AreasLoader(updateAreasProgress);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightGreen,
      // The background has been filled black
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Card(
          // Draw the card 100px away from the top
          margin: EdgeInsets.only(top: 100.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0),
              topRight: Radius.circular(20.0),
            ),
          ),
          child: FractionallySizedBox(
            widthFactor: 1.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                // Profile image
                Center(
                  child: Container(
                    transform: Matrix4.translationValues(0.0, -50.0, 0.0),
                    child: Column(
                      children: <Widget>[
                        FractionallySizedBox(
                          widthFactor: .35,
                          child: Container(
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: Image.asset('images/batman.png'),
                            ),
                          ),
                        ),
                        // Welcome message
                        Text(
                          'Welcome back, ${widget.username}',
                          style: TextStyle(
                            fontSize: 22.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                // Contents
                FractionallySizedBox(
                  widthFactor: 1.0,
                  child: Container(
                    transform: Matrix4.translationValues(0.0, -30.0, 0.0),
                    child: Column(
                      children: <Widget>[
                        // Last completed path
                        FractionallySizedBox(
                          widthFactor: 1.0,
                          child: Card(
                            elevation: 7,
                            margin: EdgeInsets.only(
                              left: 15.0,
                              right: 15.0,
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            child: Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Last completed path name',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(fontSize: 18),
                                  ),
                                  Text('6c+'),
                                  FlatButton(
                                    onPressed: () {},
                                    child: Text(
                                      'View',
                                      style: TextStyle(color: Colors.blue),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),

                        Text("Progress: $areasDownloadProgress"),

                        FutureBuilder<List<Area>>(
                          future: areasLoader.getAreas(),
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              List<Area> areas = snapshot.data;
                              print("Got areas! Count: ${areas.length}");
                              return ListView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: areas.length,
                                scrollDirection: Axis.vertical,
                                itemBuilder: (BuildContext context, int index) {
                                  Area area = areas[index];
                                  return Card(
                                    elevation: 7,
                                    margin: EdgeInsets.only(
                                      left: 15.0,
                                      right: 15.0,
                                      bottom: 15.0,
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.0)),
                                    ),
                                    semanticContainer: true,
                                    clipBehavior: Clip.antiAliasWithSaveLayer,
                                    child: Image.network(
                                      area.image,
                                      fit: BoxFit.fill,
                                    ),
                                  );
                                },
                              );
                            } else {
                              print(
                                  "Didn't get any area (Snapshot doesn't have data).");
                              return Center(child: CircularProgressIndicator());
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: ConvexAppBar(
        top: -15.0,
        backgroundColor: Colors.lightGreen.shade700,
        items: [
          TabItem(icon: Icons.download_rounded, title: 'Downloads'),
          TabItem(icon: Icons.explore_rounded, title: 'Explore'),
          TabItem(icon: Icons.settings_rounded, title: 'Settings'),
        ],
        initialActiveIndex: 1, //optional, default as 0
        onTap: (int i) => print('click index=$i'),
      ),
    );
  }
}
