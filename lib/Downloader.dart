import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

class Downloader {
  String url;
  String fileName;
  Function onProgressUpdate;

  Downloader(url, fileName, onProgressUpdate);

  String _progress = "-";
  final Dio _dio = Dio();

  Future<bool> _requestPermissions() async {
    var permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.storage);

    if (permission != PermissionStatus.granted) {
      await PermissionHandler().requestPermissions([PermissionGroup.storage]);
      permission = await PermissionHandler()
          .checkPermissionStatus(PermissionGroup.storage);
    }

    return permission == PermissionStatus.granted;
  }

  void _onReceiveProgress(int received, int total) {
    if (total != -1) {
      _progress = (received / total * 100).toStringAsFixed(0) + "%";
      onProgressUpdate(_progress);
    }
  }

  Future<void> _startDownload(String savePath) async {
    final response = await _dio.download(url, savePath,
        onReceiveProgress: _onReceiveProgress);
  }

  Future<void> download() async {
    print("Getting temp dir...");
    final dir = await getTemporaryDirectory();
    print("Requesting permissions...");
    final isPermissionStatusGranted = await _requestPermissions();

    if (isPermissionStatusGranted) {
      print("Starting the download...");
      final savePath = path.join(dir.path, fileName);
      await _startDownload(savePath);
    } else {
      // handle the scenario when user declines the permissions
      throw ("Permission is needed");
    }
  }
}
