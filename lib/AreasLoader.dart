import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Downloader.dart';
import 'package:flutter_app/podo/Area.dart';
import 'package:http/http.dart';

class AreasLoader {
  final Function progressCallback;

  AreasLoader(this.progressCallback);

  Future<List<Area>> getAreas() async {
    print("Downloading areas...");
    Downloader downloader = Downloader(
        "https://api.arnyminerz.com/area/-1", "areas.json", progressCallback);

    await downloader.download();

    return List();

    /*Response res = await get(areasURL);

    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body) as List;

      /*List<Area> areas = List();
      body.forEach((element) {
        print(" - Got area ($element)! Parsing...");
        Area area = Area.fromJSON(element);
        print("   Adding area...");
        areas.add(area);
      });*/
      List<Area> areas = body
          .map(
            (dynamic item) => Area.fromJSON(item),
          )
          .toList();

      return areas;
    } else {
      throw "Can't get posts.";
    }*/
  }
}
